# Spherical POV Display Host

## General
This is the Python Host program for the Spherical POV Display. It reads images (.png or .jpg, 100px x 112px) from a /content directory, converts them and passes the converted images into the shared memory. The script runs on a RaspberryPi Zero W.

## Requirements
The Host script requires the Python Image Library Pillow (https://python-pillow.org/) and the SysV IPC Library (http://semanchuk.com/philip/sysv_ipc/).

## Commands
There are commands defined to control the host program via the CLI:
### "reload"
Checks for new images and converts them into the display format.
### "rotate"
Toggles the rotation of the image around the sphere (pseudo animation).
### "next"
Loads the next image into the buffer of the LED Driver.
### "timer"
Toggles a timer that automatically loads the next image after 30 seconds.